require("coffee-script/register");
var webpack = require("webpack");
var starter = require("leitjohn-webpack-starter/index.coffee");

var config = starter.configure({
    production: false,
    dest: "../static/app/"
});

// config.externals = {"jquery": "jQuery"};
config.resolve.alias = {
  // 'polymer': require.resolve('polymer'),
  'jquery': require.resolve('jquery'),
  'backbone': require.resolve('backbone'),
  'backbone_views': require.resolve('backbone_views'),
};

config.plugins.push(new webpack.OldWatchingPlugin());

module.exports = config;
