var Backbone = require("backbone");
var bv = require("backbone_views");
var Provider = require("../models/provider");
var _ = require("underscore");


class ProviderItem extends bv.DetailView {
  get tagName() { return "li"; }
  get template() {
    return _.template('<a data-name data-link="href"></a>');
  }
}


class ProviderList extends bv.ListView {
  get back() { return {title: "Dashboard", url: "#/"}; }
  get template() {
    return _.template(`
      <h2>Providers</h2>
      <ul class="list"></ul>
      <!-- <div class="text-right">
        <a href="#providers/create/" class="btn btn-default">
          <i class="fa fa-plus-circle"></i>
          Create Provider
        </a>
      </div> -->
    `);
  }
  get itemViewClass() {
    return ProviderItem;
  }
  get collection() {
    return Backbone.app.collections.providers;
  }

  initialize(options) {
    super.initialize(options);

    if (this.collection.length === 0) {
      this.collection.fetch();
    }
  }
}


module.exports = ProviderList
