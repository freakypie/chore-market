var $ = require("jquery");
var bv = require("backbone_views");


class UserReward extends require("./base") {
  get slug() {
    return "user-rewards";
  }
  get urlRoot() {
    return "user-rewards";
  }
}


module.exports = UserReward;
