var $ = require("jquery");
var bv = require("backbone_views");


class Task extends require("./base") {
  get slug() {
    return "tasks";
  }
  get urlRoot() {
    return "tasks";
  }
  static get defaults() {
    return {
      name: " ---- ",
      description: ""
    }
  }

  initialize(data) {
    super.initialize(data);
  }

  set(data) {
    if (data.id) {
      data.link = `#task/${data.id}/`;
    }
    console.log('setting task', data.name);
    super.set(data);
  }

  progress() {
    return this.post("complete");
  }
}


module.exports = Task;
