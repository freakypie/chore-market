from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from rest.views import ProviderViewSet, TaskViewSet, UserRewardViewSet
from rest_framework.authtoken import views
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
router.register(r'providers', ProviderViewSet, base_name="providers")
router.register(r'tasks', TaskViewSet, base_name="tasks")
router.register(r'user-rewards', UserRewardViewSet, base_name="user-rewards")


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/token-auth/', views.obtain_auth_token),
    url(r'^api/', include(router.urls, "api", "api")),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.STATIC_ROOT,
        'show_indexes': True
    }),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT,
        'show_indexes': True
    }),
]
