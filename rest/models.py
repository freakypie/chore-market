import datetime

from dateutil import rrule

from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import MultipleObjectsReturned
from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible

User = settings.AUTH_USER_MODEL


@python_2_unicode_compatible
class Profile(models.Model):
    user = models.ForeignKey(User, null=True, blank=True)

    def __str__(self):
        return self.user.get_full_name()


@python_2_unicode_compatible
class Provider(models.Model):
    name = models.CharField(max_length=255)
    created_by = models.ForeignKey(User, related_name="my_providers")
    public = models.BooleanField(default=False)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Currency(models.Model):

    class Meta(object):
        verbose_name_plural = "Currencies"

    name = models.CharField(max_length=255)
    backend = models.CharField(max_length=100, choices=(
        ("paypal", "PayPal"), ("points", "Points"), ("dollars", "Dollars")))
    virtual = models.BooleanField(default=True)
    provider = models.ForeignKey(Provider)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Status(models.Model):
    name = models.CharField(max_length=255)
    provider = models.ForeignKey(Provider, related_name="statuses")
    complete = models.BooleanField(
        default=False,
        help_text="this status is considered complete")
    default = models.BooleanField(
        default=False,
        help_text="New Tasks will be created with this status")
    next = models.ForeignKey(
        "self", blank=True, null=True, on_delete=models.SET_NULL)
    ordering = models.SmallIntegerField(default=0)

    class Meta(object):
        ordering = ("ordering",)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Subscription(models.Model):
    user = models.ForeignKey(User, related_name="subscriptions")
    provider = models.ForeignKey(Provider, related_name="subscriptions")
    # v2: statuses = models.ManyToManyField(Status)

    # class Meta(object):
    #     unique_together = (("user", "provider"),)

    def __str__(self):
        return u'{} => {}'.format(self.user.get_full_name(), self.provider.name)




@python_2_unicode_compatible
class Task(models.Model):
    REPEATS = (
        (rrule.DAILY, "Daily"),
        (rrule.WEEKLY, "Weekly"),
        (rrule.MONTHLY, "Monthly"),
        (rrule.YEARLY, "Yearly"),
    )

    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    provider = models.ForeignKey(Provider)
    available = models.BooleanField(default=False, editable=False)
    complete = models.BooleanField(default=False, editable=False)
    status = models.ForeignKey(
        Status, null=True, blank=True, on_delete=models.SET_NULL)

    repeats = models.IntegerField(
        blank=True, null=True, choices=REPEATS)
    starting = models.DateTimeField(blank=True, null=True)
    until = models.DateTimeField(blank=True, null=True)
    interval = models.IntegerField(default=1)

    next = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    def progress(self, user=None):
        """ assigns the next status """

        if self.status and self.status.next:
            self.status = self.status.next
            self.save()

        if not user.is_anonymous():
            if not self.status.complete:
                if not self.assignments.filter(user=user):
                    self.assignments.create(
                        user=user,
                    )
            else:
                # add reward to assigned
                for assignment in self.assignments.all():
                    for reward in self.rewards.all():
                        assignment.user.rewards.create(
                            task=self,
                            currency=reward.currency,
                            amount=reward.amount
                        )

    def get_rrule(self):
        if not self.starting:
            self.starting = timezone.now()
        return rrule.rrule(
            self.repeats,
            dtstart=self.starting,
            interval=self.interval
        )

    def save(self, **kwargs):
        now = timezone.now()
        if self.repeats and (not self.until or now < self.until):
            if not self.next or self.next < now:
                self.starting = self.next
                self.next = self.get_rrule().after(self.starting)
                try:
                    self.status = self.provider.statuses.get(default=True)
                except (Status.DoesNotExist, MultipleObjectsReturned):
                    self.status = None

        super().save(**kwargs)


@python_2_unicode_compatible
class Assignment(models.Model):
    user = models.ForeignKey(User)
    task = models.ForeignKey(Task, related_name="assignments")
    changed_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.get_full_name()


@python_2_unicode_compatible
class Event(models.Model):
    """ Keeps track of changes to the tasks """
    task = models.ForeignKey(Task, related_name="events")
    notes = models.CharField(max_length=255, blank=True, null=True)
    status = models.ForeignKey(Status, blank=True, null=True)
    changed_on = models.DateTimeField(auto_now_add=True)
    changed_by = models.ForeignKey(User, blank=True, null=True)

    def __str__(self):
        return self.status


@python_2_unicode_compatible
class TaskReward(models.Model):
    task = models.ForeignKey(Task, related_name="rewards")
    currency = models.ForeignKey(Currency)
    amount = models.DecimalField(max_digits=15, decimal_places=3)

    def __str__(self):
        return u"%s %s %s".format(self.task.name, self.currency, self.amount)


@python_2_unicode_compatible
class UserReward(models.Model):
    task = models.ForeignKey(Task)
    user = models.ForeignKey(User, related_name="rewards")
    currency = models.ForeignKey(Currency)
    amount = models.DecimalField(max_digits=15, decimal_places=3)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return u"%s %s %s".format(self.task.name, self.currency, self.amount)


# v2
# class TaskSchedule(models.Model):
#     task = models.OneToOneField(Task)
#     repeat_by = Day, Week, Month
#     repeat_every = x
#     until = date
#     repeat_max = number
#
#     schedule = models.TextField(blank=True, null=True)
# class TaskItem(models.Model):
#     task = models.ForeignKey(TaskPhase)
#     description = models.CharField(max_length=255, blank=True, null=True)
# class TaskItemStatus(models.Model):
#     item = models.ForeignKey(TaskItem)
#     status = models.ForeignKey(Status)
#     completed_on = models.DateTimeField(blank=True, null=True)
#     completed_by = models.ForeignKey(Profile, blank=True, null=True)
# class Timecard(models.Model):
#     task = models.ForeignKey(Task, blank=True, null=True)
#     item = models.ForeignKey(TaskPhaseItem, blank=True, null=True)
#
#     profile = models.ForeignKey(Profile)
#     started_on = models.DateTimeField()
#     ended_on = models.DateTimeField(blank=True, null=True)
#     notes = models.TextField(blank=True, null=True)
