# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('rest', '0009_auto_20151015_2236'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserReward',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('amount', models.DecimalField(max_digits=15, decimal_places=3)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.AlterModelOptions(
            name='currency',
            options={'verbose_name_plural': 'Currencies'},
        ),
        migrations.AlterField(
            model_name='assignment',
            name='task',
            field=models.ForeignKey(related_name='assignements', to='rest.Task'),
        ),
        migrations.AlterField(
            model_name='taskreward',
            name='task',
            field=models.ForeignKey(related_name='rewards', to='rest.Task'),
        ),
        migrations.AddField(
            model_name='userreward',
            name='currency',
            field=models.ForeignKey(to='rest.Currency'),
        ),
        migrations.AddField(
            model_name='userreward',
            name='task',
            field=models.ForeignKey(to='rest.Task'),
        ),
        migrations.AddField(
            model_name='userreward',
            name='user',
            field=models.ForeignKey(related_name='rewards', to=settings.AUTH_USER_MODEL),
        ),
    ]
