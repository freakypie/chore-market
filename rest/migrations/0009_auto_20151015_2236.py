# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0008_auto_20151015_2235'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='repeats',
            field=models.IntegerField(blank=True, null=True, choices=[(3, 'Daily'), (2, 'Weekly'), (1, 'Monthly'), (0, 'Yearly')]),
        ),
    ]
