# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0007_auto_20151013_2155'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='interval',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='task',
            name='next',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='task',
            name='repeats',
            field=models.IntegerField(blank=True, null=True, choices=[(3, 'Daily'), (2, 'Weekly'), (1, 'Monthly'), (0, 'Yearly')], max_length=1),
        ),
        migrations.AddField(
            model_name='task',
            name='starting',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='task',
            name='until',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='task',
            field=models.ForeignKey(related_name='events', to='rest.Task'),
        ),
    ]
