# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0003_auto_20151011_2022'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='status',
            options={'ordering': ('default', 'complete')},
        ),
        migrations.AddField(
            model_name='status',
            name='ordering',
            field=models.SmallIntegerField(default=0),
        ),
    ]
