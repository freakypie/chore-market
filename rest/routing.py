import json

from django.contrib import auth
from django.contrib.auth import login
from django.contrib.auth.models import User

from channels import Channel, Group
from channels.auth import (channel_session_user, http_session_user,
                           transfer_user)
from channels.decorators import channel_session, http_session, linearize


channel_routing = {
    "websocket.connect": "rest.routing.ws_add",
    "websocket.keepalive": "rest.routing.ws_keepalive",
    "websocket.receive": "rest.routing.ws_message",
    "websocket.disconnect": "rest.routing.ws_disconnect",
}


# Connected to websocket.connect
@linearize
@http_session_user
@channel_session
def ws_add(message):
    print("*** add")
    # Copy user from HTTP to channel session
    if message.http_session:
        transfer_user(message.http_session, message.channel_session)
        # Add them to the right group
    Group("chat").add(message.reply_channel)


# Connected to websocket.keepalive
@channel_session_user
def ws_keepalive(message):
    print("*** keepalive")
    # Keep them in the right group
    Group("chat").add(message.reply_channel)
    Group("chat").send(
        dict(content=json.dumps(["alive", "You are alive"]))
    )


def authorize(message, token):
    # message.user = auth.get_user(fake_request)
    message.user = User.objects.get(auth_token__key=token)
    message.user.backend = 'django.contrib.auth.backends.ModelBackend'

    message.channel_session[auth.SESSION_KEY] = message.user.pk
    message.channel_session[auth.BACKEND_SESSION_KEY] = message.user.backend
    message.channel_session[auth.HASH_SESSION_KEY] = \
        message.user.get_session_auth_hash()

    Group("chat").send(
        dict(content=json.dumps(["authorized", "You are authorized"]))
    )

    # message.reply_channel.send(
    #     dict(content=json.dumps(["authorized", "You are authorized"]))
    # )

    print("****** authorized", message.user, message.channel_session.session_key)


def default_func(message, data):
    print("****** default", data)


actions = {
    "authorize": authorize,
    "default": default_func
}

# Connected to websocket.receive
@linearize
@channel_session_user
def ws_message(message):
    event, data = json.loads(message.content['content'])
    print("*** message", event, data, message.user)
    actions.get(event, default_func)(message, data)
    # Group("chat").send(dict(content="hey"))
    Group("chat").send(dict(content=json.dumps(["authorized", "You are authorized"])))


# Connected to websocket.disconnect
@channel_session_user
def ws_disconnect(message):
    print("*** disconnect")
    Group("chat").discard(message.reply_channel)
